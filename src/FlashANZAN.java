import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class FlashANZAN extends Application {
	private RadioButton rb_oneDigit, rb_twoDigits, rb_threeDigits, rb_fiveNumsSum, rb_sevenNumsSum, rb_tenNumsSum,
			rb_superLowSpeed, rb_lowSpeed, rb_standardSpeed, rb_highSpeed, rb_superHighSpeed;
	private ToggleGroup tg_digit, tg_times, tg_speed;
	private Label lb_setting, lb_answer, lb_result;
	private Button bt_start, bt_check;
	private TextField tf_answer;
	GraphicsContext gc;
	String strNum;
	int num;
	int count;
	int sum;
	int digit = 2;
	int times = 14;
	int speed = 500;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		rb_oneDigit = new RadioButton("1～9  ");
		rb_twoDigits = new RadioButton("1～99  ");
		rb_threeDigits = new RadioButton("1～999  ");
		rb_fiveNumsSum = new RadioButton("5数の和  ");
		rb_sevenNumsSum = new RadioButton("7数の和  ");
		rb_tenNumsSum = new RadioButton("10数の和  ");
		rb_superLowSpeed = new RadioButton("超低速  ");
		rb_lowSpeed = new RadioButton("低速  ");
		rb_standardSpeed = new RadioButton("標準  ");
		rb_highSpeed = new RadioButton("高速  ");
		rb_superHighSpeed = new RadioButton("超高速  ");

		tg_digit = new ToggleGroup();
		tg_times = new ToggleGroup();
		tg_speed = new ToggleGroup();

		rb_oneDigit.setToggleGroup(tg_digit);
		rb_twoDigits.setToggleGroup(tg_digit);
		rb_threeDigits.setToggleGroup(tg_digit);
		rb_fiveNumsSum.setToggleGroup(tg_times);
		rb_sevenNumsSum.setToggleGroup(tg_times);
		rb_tenNumsSum.setToggleGroup(tg_times);
		rb_superLowSpeed.setToggleGroup(tg_speed);
		rb_lowSpeed.setToggleGroup(tg_speed);
		rb_standardSpeed.setToggleGroup(tg_speed);
		rb_highSpeed.setToggleGroup(tg_speed);
		rb_superHighSpeed.setToggleGroup(tg_speed);

		rb_twoDigits.setSelected(true);
		rb_sevenNumsSum.setSelected(true);
		rb_standardSpeed.setSelected(true);

		rb_oneDigit.setOnAction(new DigitSettingEventHandler());
		rb_twoDigits.setOnAction(new DigitSettingEventHandler());
		rb_threeDigits.setOnAction(new DigitSettingEventHandler());
		rb_fiveNumsSum.setOnAction(new TimesSettingEventHandler());
		rb_sevenNumsSum.setOnAction(new TimesSettingEventHandler());
		rb_tenNumsSum.setOnAction(new TimesSettingEventHandler());
		rb_superLowSpeed.setOnAction(new SpeedSettingEventHandler());
		rb_lowSpeed.setOnAction(new SpeedSettingEventHandler());
		rb_standardSpeed.setOnAction(new SpeedSettingEventHandler());
		rb_highSpeed.setOnAction(new SpeedSettingEventHandler());
		rb_superHighSpeed.setOnAction(new SpeedSettingEventHandler());

		lb_setting = new Label("設定入力⇒");
		lb_answer = new Label("合計入力⇒");
		lb_result = new Label();

		bt_start = new Button("スタート");
		bt_check = new Button("答え合わせ");

		bt_start.setOnAction(new FlashExecuteEventHandler());
		bt_check.setOnAction(new AnswerCheckEventHandler());

		tf_answer = new TextField();

		Canvas canvas = new Canvas(600, 300);

		gc = canvas.getGraphicsContext2D();

		HBox hb_digit = new HBox();
		HBox hb_times = new HBox();
		HBox hb_speed = new HBox();
		HBox hb_topStartSetting = new HBox();
		HBox hb_bottomAnswerCheck = new HBox();
		VBox vb_settingElement = new VBox();

		hb_digit.getChildren().addAll(rb_oneDigit, rb_twoDigits, rb_threeDigits);
		hb_times.getChildren().addAll(rb_fiveNumsSum, rb_sevenNumsSum, rb_tenNumsSum);
		hb_speed.getChildren().addAll(rb_superLowSpeed, rb_lowSpeed, rb_standardSpeed, rb_highSpeed, rb_superHighSpeed);
		vb_settingElement.getChildren().addAll(hb_digit, hb_times, hb_speed);
		hb_topStartSetting.getChildren().addAll(lb_setting, vb_settingElement, bt_start);
		hb_bottomAnswerCheck.getChildren().addAll(lb_answer, tf_answer, bt_check, lb_result);

		hb_topStartSetting.setAlignment(Pos.CENTER_LEFT);
		hb_bottomAnswerCheck.setAlignment(Pos.CENTER_LEFT);

		BorderPane bp = new BorderPane();

		bp.setTop(hb_topStartSetting);
		bp.setCenter(canvas);
		bp.setBottom(hb_bottomAnswerCheck);

		Scene scene = new Scene(bp, 600, 400);

		primaryStage.setScene(scene);
		primaryStage.setTitle("フラッシュ暗算");
		primaryStage.show();
	}

	void initialize() {
		count = 0;
		sum = 0;

		gc.clearRect(0, 0, 600, 300);
		gc.setFont(new Font(250));

		tf_answer.setText("");
		lb_result.setText("");
	}

	class DigitSettingEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			RadioButton tmp = (RadioButton)event.getSource();

			if(tmp == rb_twoDigits) {
				digit = 2;
			} else if(tmp == rb_oneDigit) {
				digit = 1;
			} else if(tmp == rb_threeDigits) {
				digit = 3;
			}
		}
	}

	class TimesSettingEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			RadioButton tmp = (RadioButton)event.getSource();

			if(tmp == rb_sevenNumsSum) {
				times = 14;
			} else if(tmp == rb_fiveNumsSum) {
				times = 10;
			} else if(tmp == rb_tenNumsSum) {
				times = 20;
			}
		}
	}

	class SpeedSettingEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			RadioButton tmp = (RadioButton)event.getSource();

			if(tmp == rb_standardSpeed) {
				speed = 500;
			} else if(tmp == rb_superLowSpeed) {
				speed = 1500;
			} else if(tmp == rb_lowSpeed) {
				speed = 1000;
			} else if(tmp == rb_highSpeed) {
				speed = 250;
			} else if(tmp == rb_superHighSpeed) {
				speed = 150;
			}
		}
	}

	class FlashExecuteEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			initialize();

			Timer timer = new Timer();

			TimerTask task = new TimerTask() {
				@Override
				public void run() {
					count++;

					if(count % 2 == 1) {
						switch(digit) {
							case 1:
								num = (int)(Math.random() * 8) + 1;
								break;
							case 2:
								num = (int)(Math.random() * 98) + 1;
								break;
							case 3:
								num = (int)(Math.random() * 998) + 1;
								break;
							default:
								break;
						}

						strNum = "" + num;

						if(num <= 9) {
							gc.fillText(strNum, 210, 230);
						} else if(num <= 99) {
							gc.fillText(strNum, 130, 230);
						} else {
							gc.fillText(strNum, 65, 230);
						}

						sum += num;
					} else {
						gc.clearRect(0, 0, 600, 300);
					}

					if(count >= times) {
						gc.setFont(new Font(150));
						gc.fillText("合計は?", 50, 180);

						timer.cancel();
					}
				}
			};

			timer.scheduleAtFixedRate(task, 1000, speed);
		}
	}

	class AnswerCheckEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			int inputSum = Integer.parseInt(tf_answer.getText());

			if(inputSum == sum) {
				lb_result.setText("合計 : " + sum + "    正解です。");
			} else {
				lb_result.setText("合計 : " + sum + "    不正解です。");
			}
		}
	}
}
